﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AmadeusProject.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Amadeus.Service.Interfaces;
using Amadeus.Service.Services;
using Amadeus.Data.Models;
using Microsoft.AspNetCore.Http;

namespace AmadeusProject.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserService _userService;

        public HomeController()
        {
            _userService = _userService ?? new UserService();
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult Login(AccountBindingModel model)
        {
            if (ModelState.IsValid)
            {
                var result = _userService.GetUser(new User() { Username = model.Username, Password = model.Password});

                if (result.ResultStatus)
                {
                    CookieOptions options = new CookieOptions
                    {
                        Expires = DateTime.Now.AddDays(1)
                    };
                    Response.Cookies.Append("username", model.Username, options);

                    return RedirectToAction(nameof(CalcController.Index), "Calc");
                }
                else
                {
                    return View("Index");
                }
            }

            return View("Index",model);
        }

        public IActionResult LogOff()
        {
            Response.Cookies.Delete("username");
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }
        
        private IActionResult RedirectToReturnUrl(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
