﻿using Amadeus.Data.Models;
using Amadeus.Service;
using Amadeus.Service.Interfaces;
using Amadeus.Service.Services;
using AmadeusProject.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AmadeusProject.Web.Controllers
{

    public class CalcController : Controller
    {
        private readonly IOperationService _operationService;
        private readonly SelectList _operators;
        public CalcController()
        {
            _operationService = _operationService ?? new OperationService();
            _operators = Functions.GetSelectList();
        }

        public IActionResult Index()
        {
            if (string.IsNullOrEmpty(Request.Cookies["username"]))
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Operators = _operators;
            return View();
        }

        [HttpPost]
        public IActionResult Calculate(CalculationBindingModel model)
        {
            ViewBag.Operators = _operators;
            ModelState.Clear();

            if (Request.Form["clearButton"].Count > 0)
            {
                return View("Index");
            }
           
            //TODO: Automapper
            model.Result = _operationService.Calculate(new CalculationModel()
            {
                FirstNumber = model.FirstNumber,
                SecondNumber = model.SecondNumber,
                CalculationMethod = model.CalculationMethod
            });

            return View("Index", model);
        }

        [HttpPost]
        public IActionResult Clear()
        {
            ModelState.Clear();
            return View("Index");
        }
    }
}
