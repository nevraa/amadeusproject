﻿using Amadeus.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AmadeusProject.Web.Models
{
    public class CalculationBindingModel
    {
        [RegularExpression(@"^[0-9]{1,2}([,.][0-9]{1,2})?$", ErrorMessage = "Invalid number")]
        public decimal FirstNumber { get; set; }
        [RegularExpression(@"^[0-9]{1,2}([,.][0-9]{1,2})?$", ErrorMessage = "Invalid number")]
        public decimal SecondNumber { get; set; }
        public decimal Result { get; set; } = 0;
        public CalculationType CalculationMethod { get; set; }
    }
}
