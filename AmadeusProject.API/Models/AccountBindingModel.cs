﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AmadeusProject.API.Models
{
    public class AccountBindingModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
