﻿using Microsoft.AspNetCore.Mvc;
using AmadeusProject.API.Models;
using Amadeus.Data.Models;
using System;
using Amadeus.Service.Interfaces;

namespace AmadeusProject.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Account")]
    public class AccountController : Controller
    {
        private readonly IUserService _userService;

        public AccountController(IUserService userService)
        {
            _userService = userService;
        }

        [Route("register")]
        [HttpPost]
        public RegisterViewModel Register([FromBody] AccountBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return new RegisterViewModel();
            }

            var result = _userService.CreateUser(new User()
            {
                Id = Guid.NewGuid().ToString(),
                Username = model.Username,
                Password = model.Password
            });

            return new RegisterViewModel()
            {
                Username = result.Username,
            };
        }

        [Route("login")]
        [HttpPost]
        public IActionResult Login([FromBody] AccountBindingModel model)
        {
            return Ok();
        }
    }
}