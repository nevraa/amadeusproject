﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Amadeus.Service.Interfaces;
using AmadeusProject.API.Models;
using Amadeus.Data.Models;

namespace AmadeusProject.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Operation")]
    public class OperationController : Controller
    {
        private readonly IOperationService _operationService;

        public OperationController(IOperationService operationService)
        {
            _operationService = operationService;
        }

        [HttpPost]
        public decimal Post([FromBody] CalculationModel model)
        {
            var result = _operationService.Calculate(model);
            return result;
        }
    }
}