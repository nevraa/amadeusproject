﻿using Amadeus.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Amadeus.Data.Models;
using Amadeus.Data;

namespace Amadeus.Service.Services
{
    public class OperationService : IOperationService
    {
        public decimal Calculate(CalculationModel model)
        {
            decimal answer = 0M;
            switch (model.CalculationMethod)
            {
                case CalculationType.Add:
                    answer = model.FirstNumber + model.SecondNumber;
                    break;
                case CalculationType.Subtract:
                    answer = model.FirstNumber - model.SecondNumber;
                    break;
                case CalculationType.Multiply:
                    answer = model.FirstNumber * model.SecondNumber;
                    break;
                case CalculationType.Divide:
                    answer = model.SecondNumber == 0 ?
                        0M :
                        Math.Round(model.FirstNumber / model.SecondNumber, 2);
                    break;
                default:
                    break;
            }

            return answer;
        }
    }
}
