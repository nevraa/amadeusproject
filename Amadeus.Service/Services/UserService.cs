﻿using Amadeus.Data;
using Amadeus.Data.Models;
using Amadeus.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Amadeus.Service.Services
{
    public class UserService : IUserService
    {
        private readonly UnitOfWork _unitOfWork;

        public UserService()
        {
            _unitOfWork = new UnitOfWork();
        }

        public BaseModel<User> CreateUser(User user)
        {
            var result = _unitOfWork.UserRepository.Insert(user);

            if (result != null)
            {
                return new BaseModel<User>()
                {
                    Result = result,
                };
            }
            else
            {
                return new BaseModel<User>()
                {
                    ResultStatus = false,
                    Error = new ErrorModel()
                    {
                        Description = "An error occured."
                    }
                };
            }
        }

        public BaseModel<User> GetUser(User user)
        {
            var result = _unitOfWork.UserRepository.GetFirst(u => u.Username == user.Username);

            if (result != null && user.Password.Equals(result.Password))
            {
                return new BaseModel<User>()
                {
                    Result = result
                };
            }
            else
            {
                return new BaseModel<User>()
                {
                    ResultStatus = false,
                    Error = new ErrorModel()
                    {
                        Description = "Please check your credentials"
                    }
                };
            }
        }
    }
}
