﻿using Amadeus.Data;
using Amadeus.Data.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Amadeus.Service
{
    public static class Functions
    {
        public static SelectList GetSelectList()
        {
            return new SelectList(Enum.GetValues(typeof(CalculationType)).Cast<CalculationType>().ToList());
        }

    }
}
