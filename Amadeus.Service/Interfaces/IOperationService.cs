﻿using Amadeus.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Amadeus.Service.Interfaces
{
    public interface IOperationService
    {
        decimal Calculate(CalculationModel model);
    }
}
