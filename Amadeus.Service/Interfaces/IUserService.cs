﻿using Amadeus.Data.Models;

namespace Amadeus.Service.Interfaces
{
    public interface IUserService
    {
        BaseModel<User> CreateUser(User user);
        BaseModel<User> GetUser(User user);
    }
}
