﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amadeus.Data.Models
{
    public class CalculationModel
    {
        public decimal FirstNumber { get; set; }
        public decimal SecondNumber { get; set; }
        public decimal Result { get; set; }
        public CalculationType CalculationMethod { get; set; }
    }
}
