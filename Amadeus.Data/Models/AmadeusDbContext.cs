﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Amadeus.Data.Models
{
    public partial class AmadeusDbContext : DbContext
    {
        public virtual DbSet<User> User { get; set; }
        public static string ConnectionString { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Server=34.250.156.85;Database=AmadeusDb;User Id=TestUser; Password=2123naNA..; MultipleActiveResultSets=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasMaxLength(128)
                    .ValueGeneratedNever();

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(8);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(8);
            });
        }
    }
}
