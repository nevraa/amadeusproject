﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amadeus.Data.Models
{
    public class BaseModel<T>
    {
        public T Result;
        public DateTime ResultDate = DateTime.Now;
        public bool ResultStatus = true;
        public ErrorModel Error;

        /// <summary>
        /// 
        /// </summary>
        public BaseModel()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        public BaseModel(T result)
        {
            Result = result;
            ResultStatus = true;
        }

    }

    public class ErrorModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
