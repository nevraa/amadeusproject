﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amadeus.Data
{
    public enum CalculationType
    {
        Add = '+',
        Subtract = '-',
        Multiply = '*',
        Divide = '/'
    }
}
