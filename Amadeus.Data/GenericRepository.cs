﻿using Amadeus.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Amadeus.Data
{
    public class GenericRepository<TEntity> where TEntity : class
    {
        internal AmadeusDbContext Context;
        internal DbSet<TEntity> DbSet;

        public GenericRepository(AmadeusDbContext context)
        {
            Context = context;
            DbSet = context.Set<TEntity>();
        }

        public virtual IEnumerable<TEntity> Get()
        {
            IQueryable<TEntity> query = DbSet;
            return query.ToList();
        }

        public TEntity GetFirst(Func<TEntity, bool> func)
        {
            return DbSet.FirstOrDefault(func);
        }

        public virtual TEntity Insert(TEntity entity)
        {
            DbSet.Add(entity);
            return entity;
        }
    }
}
