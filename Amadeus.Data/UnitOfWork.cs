﻿using Amadeus.Data.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Amadeus.Data
{
    public class UnitOfWork : IDisposable
    {
        private readonly AmadeusDbContext _amadeusDbContext = null;
        private bool _disposed = false;

        private GenericRepository<User> _userRepository;

        public UnitOfWork()
        {
            _amadeusDbContext = new AmadeusDbContext();
        }

        public GenericRepository<User> UserRepository => _userRepository ?? (_userRepository = new GenericRepository<User>(_amadeusDbContext));

        public void Save()
        {
            try
            {
                _amadeusDbContext.SaveChanges();
            }
            catch (InvalidOperationException e)
            {
                System.IO.File.AppendAllLines(@"C:\errors.txt", new List<string>() { e.InnerException.Message });
                throw;
            }

        }

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    Debug.WriteLine("UnitOfWork is being disposed");
                    _amadeusDbContext.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
